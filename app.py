import random
from flask import Flask, render_template, request, redirect, url_for, flash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired
import secrets

app = Flask(__name__)
app.secret_key = secrets.token_hex(16)  # Replace this with a secure random string

login_manager = LoginManager(app)
login_manager.login_view = 'login'

# Mock user database
class User(UserMixin):
    def __init__(self, id):
        self.id = id

users = {'user': User('user'), 'admin': User('admin')}

# WTForms LoginForm
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Log In')


@app.route('/login', methods=['GET', 'POST'])
@app.route('/', methods=['GET', 'POST'])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data

        user = users.get(username)
        if user and password == 'password':  # For simplicity, use a hardcoded password 'password'
            login_user(user)
            return redirect(url_for('home'))
        else:
            flash('Invalid username or password.', 'error')
    return render_template('login.html', form=form)

@app.route('/home')
@login_required
def home():
    return render_template('home.html')

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

@login_manager.user_loader
def load_user(user_id):
    return users.get(user_id)

@app.route('/info')
def info():
    return render_template('info.html')

@app.route('/generate', methods=['POST'])
def generate():
    if request.method == 'POST':
        min_value = int(request.form['min_value'])
        max_value = int(request.form['max_value'])
        if min_value > max_value:
            error_message = 'Maximum value must be greater than Minimum Value!'
            return render_template('home.html', error_message=error_message)
        else:
            random_number = random.randint(min_value, max_value)
            return render_template('generate.html', min_value=min_value, max_value=max_value, random_number=random_number)
