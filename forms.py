from flask_wtf import FlaskForm
from wtforms import IntegerField, SubmitField, StringField

class ParametersForm(FlaskForm):
    number_min = IntegerField(label='Min:')
    number_max = IntegerField(label='Max:')
    submit = SubmitField(label='Generate Random Number')



