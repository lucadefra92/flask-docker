import pytest
from app import app, User
from flask_login import login_user

@pytest.fixture
def client():
    app.config['TESTING'] = True
    client = app.test_client()
    yield client

@pytest.fixture
def authenticated_client(client):
    with app.test_request_context():
        user = User(id='user')  # Assuming User is your user class
        login_user(user)
    return client
