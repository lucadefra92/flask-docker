from app import app
from flask_login import login_user
from app import User

def test_login(client):
    response = client.get('/login')
    assert response.status_code == 200

def authenticated_client(client):
    with app.test_request_context():
        user = User(id='user')  # Assuming User is your user class
        login_user(user)
    return client

def test_home(client):
    response = client.get('/home')
    assert response.status_code == 302  # Expecting redirect status myapp because user is not logged in

def test_logout(client):
    response = client.get('/logout')
    assert response.status_code == 302  # Expecting redirect status myapp because user is not logged in

def test_info(client):
    response = client.get('/info')
    assert response.status_code == 200

def test_generate(client):
    response = client.post('/generate', data={'min_value': '1', 'max_value': '100'})
    assert response.status_code == 200

def test_generate_valid_input(client):
    response = client.post('/generate', data={'min_value': '123', 'max_value': '567'})
    assert response.status_code == 200
    assert b'123' in response.data
    assert b'567' in response.data

def test_generate_invalid_input(client):
    response = client.post('/generate', data={'min_value': '100', 'max_value': '1'})
    assert response.status_code == 200
    assert b'Maximum value must be greater than Minimum Value!' in response.data
