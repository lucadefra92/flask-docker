import boto3
from botocore.config import Config

# Define parameters
region = 'us-east-1'
repo_uri = 'public.ecr.aws/g1q4u2w0/lucadefra92/flask-random-number-app'

my_config = Config(
    region_name = region,
    signature_version = 'v4',
    retries = {
        'max_attempts': 10,
        'mode': 'standard'
    }
)

# Define client to interact with aws eks
ecr_client = boto3.client('ecr-public', config=my_config)


def test_ecr_repo_exists():
    repositories = ecr_client.describe_repositories()['repositories']
    repo_uris = [repo['repositoryUri'] for repo in repositories]
    assert repo_uri in repo_uris
