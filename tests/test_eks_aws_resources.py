import boto3
from botocore.config import Config

# Define parameters
region = 'us-east-1'
eks_cluster_name = 'flask-cluster'
eks_node_group_name = 'flask-cluster-node-group'

my_config = Config(
    region_name = region,
    signature_version = 'v4',
    retries = {
        'max_attempts': 10,
        'mode': 'standard'
    }
)

# Define client to interact with aws eks
eks_client = boto3.client('eks', config=my_config)


def test_eks_cluster_exists():
    response = eks_client.describe_cluster(name=eks_cluster_name)
    assert response['cluster']['status'] == 'ACTIVE'

def test_eks_cluster_node_group_exists():
    response = eks_client.describe_nodegroup(clusterName=eks_cluster_name, nodegroupName=eks_node_group_name)
    assert response['nodegroup']['status'] == 'ACTIVE'

